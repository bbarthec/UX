import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { Observable } from 'rxjs/Observable';
import { ProjectItem } from '../models/ProjectItem.model';

@Component({
  selector: 'ux-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  projects: Observable<ProjectItem[]>;

  type = 'all';
  order = '';

  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.projects = this.projectService.getProjects();
  }

  orderBy(param: string): void {
    this.order = param;
    this.projects = this.projectService.getProjects(this.type, this.order);
  }

  filter(param: string): void {
    if (this.type === param) {
      this.type = 'all';
    } else {
      this.type = param;
    }
    if (this.order !== '') {
      this.projects = this.projectService.getProjects(this.type, this.order);
    } else {
      this.projects = this.projectService.getProjects(this.type);
    }
  }

}
