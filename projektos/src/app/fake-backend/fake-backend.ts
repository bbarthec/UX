import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';
import { ProjectItemDetailed } from '../models/ProjectItemDetailed.model';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const projectsDetailed: ProjectItemDetailed[] = GetProjects(); // JSON.parse(localStorage.getItem('projects')) || [];

        const users: any[] = JSON.parse(localStorage.getItem('users')) || [];


        return Observable.of(null).mergeMap(() => {

            if (request.url.startsWith('api/projects/')) {
                const split = request.url.split('/');
                const id = +split[split.length - 1];

                const project = projectsDetailed.find(x => x.projectItem.id === id);
                return Observable.of(new HttpResponse({ status: 200, body: project }));
            }

            if (request.url.startsWith('api/projects') && request.method === 'GET') {
                const type = request.params.get('type');
                const orderby = request.params.get('orderby');

                let filtered = projectsDetailed.map(x => x.projectItem);
                if (type === 'your') {
                    filtered = filtered.filter(x => x.createdBy === 'Ryszard Sikora');
                } else if (type === 'other') {
                    filtered = filtered.filter(x => x.createdBy !== 'Ryszard Sikora');
                }
                if (orderby === 'name') {
                    filtered = filtered.sort((x, y) => {
                        if (x.createdBy < y.createdBy) { return -1; }
                        if (x.createdBy === y.createdBy) { return 0; }
                        return 1;
                    });
                } else if (orderby === 'updatedate') {
                    filtered = filtered.sort((x, y) => {
                        if (x.createdDate < y.createdDate) { return -1; }
                        if (x.createdDate === y.createdDate) { return 0; }
                        return 1;
                    });
                }
                return Observable.of(new HttpResponse({ status: 200, body: filtered }));
            }

            // pass through any requests not handled above
            return next.handle(request);
        })
        .materialize()
        .delay(500)
        .dematerialize();
    }
}

export let fakeBackendProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};


function GetProjects(): ProjectItemDetailed[] {
    return [
        {
            projectItem: {
                id: 1,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 2,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 3,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 4,
                title: 'Streamify',
                createdBy: 'Bartłomiej Bukowski',
                createdDate: new Date(2018, 7, 6),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 5,
                title: 'Streamify',
                createdBy: 'Bartłomiej Bukowski',
                createdDate: new Date(2018, 7, 5),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 6,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(2018, 7, 4),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 7,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(2018, 7, 3),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 8,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(2018, 7, 2),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
        {
            projectItem: {
                id: 9,
                title: 'Streamify',
                createdBy: 'Ryszard Sikora',
                createdDate: new Date(2018, 7, 1),
                logoUrl: '/assets/images/ProjectLogo.jpg'
            },
            tags: ['tag1', 'tag2'],
            // tslint:disable-next-line:max-line-length
            description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
            history: [
                {
                    content: 'Ryszard Sikora has removed Bartek Bukowski from the project!',
                    timeSection: 'week',
                    timestamp: '6 hours ago'
                }
            ],
            members: [
                {
                    role: 'owner',
                    name: 'Ryszard Sikora'
                }
            ]
        },
    ];
}

