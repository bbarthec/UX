export class ProjectItem {
    id: Number;
    title: String;
    createdBy: String;
    createdDate: Date;
    logoUrl: String;
}
