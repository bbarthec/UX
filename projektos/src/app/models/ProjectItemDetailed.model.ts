import { HistoryItem } from './HistoryItem.model';
import { Member } from './Member.model';
import { ProjectItem } from './ProjectItem.model';

export class ProjectItemDetailed {
    projectItem: ProjectItem;

    description: String;
    tags: String[];
    history: HistoryItem[];
    members: Member[];
}
