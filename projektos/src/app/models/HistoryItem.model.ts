export class HistoryItem {
    timestamp: string;
    content: string;
    timeSection: string; // this week, this month, before
}
