import { Component } from '@angular/core';

@Component({
  selector: 'ux-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ux';
}
