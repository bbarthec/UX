import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProjectItem } from '../models/ProjectItem.model';
import { Observable } from 'rxjs/Observable';
import { ProjectItemDetailed } from '../models/ProjectItemDetailed.model';

@Injectable()
export class ProjectService {

  constructor(private http: HttpClient) { }

  getProjects(type = 'all', orderby = 'name'): Observable<ProjectItem[]> {

    if (type !== 'all' && type !== 'your' && type !== 'other') {
      throw new Error('getProjects(): invalid value for parameter type: ' + type);
    }

    if (orderby !== 'name' && orderby !== 'updatedate') {
      throw new Error('getProjects(): invalid value for parameter orderby: ' + orderby);
    }

    const params = new HttpParams()
      .set('type', type)
      .set('orderby', orderby);
    const response = this.http.get<ProjectItem[]>('api/projects', {params});
    return response;
  }

  getProjectDetails(id: Number): Observable<ProjectItemDetailed> {
    const response = this.http.get<ProjectItemDetailed>('api/projects/' + id);
    return response;
  }

}
