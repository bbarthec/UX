import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { Observable } from 'rxjs/Observable';
import { switchMap, share } from 'rxjs/operators';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ProjectItemDetailed } from '../models/ProjectItemDetailed.model';

@Component({
  selector: 'ux-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.scss']
})
export class ProjectItemComponent implements OnInit {

  item$: Observable<ProjectItemDetailed>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private projectService: ProjectService) { }

  ngOnInit() {
    this.item$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => 
      this.projectService.getProjectDetails(+params.get('id'))));
  }

}
