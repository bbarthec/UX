# Project manager
### UX project - React version

## Usage

Only development currently is available.

All needed to build and launch this project:

1. install `node.js` (it comes with `npm` package manager)
2. `npm i -g yarn` (you can omit it `yarn` is already on your machine)
3. `yarn` - that would install all dependencies
4. `yarn run watch-client` - that would launch dev environment

## Project layout

It consist only of one part: `client`.

* `watch-client` - task for building client side application
