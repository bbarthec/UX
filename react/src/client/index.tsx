import React from 'react';
import ReactDOM from 'react-dom';

import Router from './views/Router';

ReactDOM.render(
  <Router />,
  document.getElementById('root') as HTMLElement,
);
