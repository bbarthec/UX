import classNames from 'classnames';
import _ from 'lodash';
import React, { ChangeEvent } from 'react';
import {
  MdArrowDropDown,
  MdArrowDropUp,
  MdClear,
  MdRemoveCircleOutline,
  MdSearch,
} from 'react-icons/lib/md';
import Select, { Option } from 'react-select';

import { Role, User } from './ProjectView';

import './TeamTab.scss';

interface Props {
  members: User[];
}

interface State {
  members: User[];
  search: string | null;
  role: Role | null;
}

export default class TeamTab extends React.PureComponent<Props, State> {
  private static defaultProps = {
    name: 'TEAM',
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      members: _.cloneDeep(props.members),
      role: null,
      search: null,
    };
  }

  public render() {
    const { search, role } = this.state;
    return (
      <div className='TeamTab'>
        <div className='TeamTab-searchBar'>
          <input
            placeholder='SEARCH FOR COLLEAGUE'
            className='TeamTab-searchInput' onChange={this.handleSearch} value={search || ''} />
          <button
            className={classNames('TeamTab-searchButton', search && 'TeamTab-searchButton--active')}
            onClick={() => search && this.setState({ search: null })}
          >
            { !search ? (
              <MdSearch />
            ) : (
              <MdClear />
            )}
          </button>
        </div>
        { !search && (
          <div className='TeamTab-dropdown'>
            <Select
              className='TeamTab-select'
              optionClassName='TeamTab-selectOption'
              placeholder='ALL ROLES'
              searchable={false}
              arrowRenderer={({ isOpen }) => isOpen ? (
                <MdArrowDropUp size={50}/>
              ) : (
                <MdArrowDropDown size={50} />
              )}
              value={role || undefined}
              onChange={this.handleRole}
              options={[
                { value: null, label: 'ALL ROLES' },
                { value: Role.owner, label: 'OWNERS' },
                { value: Role.developer, label: 'DEVELOPERS' },
                { value: Role.invited, label: 'INVITED' },
              ]}
            />
          </div>
        )}
        <ul className='TeamTab-members'>
          { this.state.members
            .filter(user => role ? user.role === role : true)
            .filter(user => search ? user.name.includes(search) : true)
            .map((user, idx) => (
              <li key={idx} className='TeamTab-member'>
                <div className='TeamTab-memberAvatar'>
                  <img src={user.avatar} alt='' />
                </div>
                <div className='TeamTab-memberName'>
                  {user.name}
                </div>
                <div className='TeamTab-memberRole'>
                  {user.role}
                </div>
                <div className='TeamTab-memberActions'>
                  <button className='TeamTab-memberRemove' onClick={() => {
                    const members = _.cloneDeep(this.state.members);
                    const memberIdx = members.findIndex(m => m.name === user.name);
                    members.splice(memberIdx, 1);
                    this.setState({ members });
                  }}>
                    <MdRemoveCircleOutline />
                  </button>
                </div>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }

  private handleSearch = (event: ChangeEvent<HTMLInputElement>) => this.setState({ search: event.target.value });

  private handleRole = (role: Option<Role | null>) => this.setState({ role: role && role.value! });
}
