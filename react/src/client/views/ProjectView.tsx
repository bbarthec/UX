import classNames from 'classnames';
import React from 'react';

import { MdPerson } from 'react-icons/lib/md';
import { Editable, Header, Tabs } from './components';

import HistoryTab from './HistoryTab';
import TeamTab from './TeamTab';

import ImageProjectLogo from '@client/assets/images/ProjectLogo.jpg';
import ImageUserAvatar from '@client/assets/images/UserAvatar.png';

import './ProjectView.scss';

export default class ProjectView extends React.PureComponent<{}, State> {
  public state = {
    project: {
      creationDate: new Date(2018, 10, 20),
      // tslint:disable-next-line:max-line-length
      description: 'Although this article will expound on using this strategy with React components, it makes sense in any ES6 project where you want to have multiple subclasses provide configuration data to a super class in the form of static props. In a React project I’m working on, we had a bunch of components doing mostly the same logic in their shouldComponentUpdate methods.In this logic, it would reference a mixture of static properties and instance methods of each component. It made sense to extract a base class and get the components to subclass it, providing an overridable method here and there for some behavior specific to the subclass in question.This is all easy enough, and works as you would expect.',
      events: [
        {
          action: Action.remove,
          date: new Date(2018, 5, 20),
          owner: 'Ryszard Sikora',
          target: 'Bartek Bukowski',
        },
        {
          action: Action.invite,
          date: new Date(2018, 5, 18),
          owner: 'Ryszard Sikora',
          target: 'Bartek Bukowski',
        },
        {
          action: Action.invite,
          date: new Date(2018, 5, 14),
          owner: 'Ryszard Sikora',
          target: 'Bartek Bukowski',
        },
        {
          action: Action.remove,
          date: new Date(2018, 5, 10),
          owner: 'Ryszard Sikora',
          target: 'Bartek Bukowski',
        },
        {
          action: Action.remove,
          date: new Date(2018, 5, 5),
          owner: 'Ryszard Sikora',
          target: 'Bartek Bukowski',
        },
      ],
      id: '#1234',
      logo: ImageProjectLogo,
      members: [{
        avatar: ImageUserAvatar,
        name: 'Ryszard Sikora',
        role: Role.owner,
      }, {
        avatar: ImageUserAvatar,
        name: 'Bartek Bukowski',
        role: Role.developer,
      }, {
        avatar: ImageUserAvatar,
        name: 'Ktoś Znany',
        role: Role.developer,
      }, {
        avatar: ImageUserAvatar,
        name: 'Nikt Niktowski',
        role: Role.invited,
      }],
      name: 'Streamify',
      owner: 'Ryszard Sikora',
      tags: [
        'Spring', 'Java', 'Angular5', 'React', 'Redux', 'ES6', 'WebServis',
      ],
    },
    tab: 'team',
  };

  public render () {
    const { logo, name, members, tags, description, events } = this.state.project;

    return (
      <div className='ProjectView'>
        <Header title='MANAGE PROJECT'/>
        <div className='ProjectView-content'>
          <div className='ProjectView-informations'>
            <div className='ProjectView-logo'>
              <img src={logo} alt='' />
            </div>
            <Editable
              value={name}
              onEdit={value => value && this.setState({ project: { ...this.state.project, name: value } })}
            >
              <div className='ProjectView-name'>{name}</div>
            </Editable>
            <div className='ProjectView-members'><MdPerson size={30} /> {members.length}</div>
            <Editable
              value={tags.join(',')}
              onEdit={value => value && this.setState({ project: { ...this.state.project, tags: value.split(',') } })}
            >
              <div className='ProjectView-tags'>
                {tags.map((tag, idx) => (
                  <div className='ProjectView-tag' key={idx}>
                    {tag}
                  </div>
                ))}
              </div>
            </Editable>
            <Editable
              value={description}
              onEdit={value => value && this.setState({ project: { ...this.state.project, tags: value.split(',') } })}
            >
              <div className='ProjectView-description'>{description}</div>
            </Editable>
          </div>
          <Tabs
            selected={this.state.tab}
            className='ProjectView-tabs'
            onTabClick={tab => this.setState({ tab })}
          >
            <TeamTab members={members} />
            <HistoryTab events={events} />
          </Tabs>
        </div>
      </div>
    );
  }
}

export enum Role {
  owner = 'OWNER',
  developer = 'DEVELOPER',
  invited = 'INVITED',
}

export enum Action {
  invite,
  remove,
}

export interface Event {
  date: Date;
  target: string;
  owner: string;
  action: Action;
}

export interface User {
  name: string;
  avatar: string;
  role: Role;
}

interface Project {
  name: string;
  owner: string;
  id: string;
  creationDate: Date;
  logo: string;
  members: User[];
  tags: string[];
  description: string;
  events: Event[];
}

interface State {
  project: Project;
  tab: string;
}
