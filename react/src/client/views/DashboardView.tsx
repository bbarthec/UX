import classNames from 'classnames';
import React from 'react';
import { MdCreateNewFolder, MdKeyboardArrowRight } from 'react-icons/lib/md';
import { NavLink } from 'react-router-dom';

import { Header } from './components';

import ImageProjectLogo from '@client/assets/images/ProjectLogo.jpg';

import './DashboardView.scss';

export default class DashboardView extends React.PureComponent<{}, State> {
  public state = {
    orderBy: OrderBy.name,
    projects: [{
      creationDate: new Date(2017, 1, 1, 22, 23),
      id: '1243',
      logo: ImageProjectLogo,
      name: '1Streamify',
      owner: 'Bartłomiej Bukowski',
    }, {
      creationDate: new Date(2019, 1, 1, 22, 23),
      id: '1243',
      logo: ImageProjectLogo,
      name: '2Streamify',
      owner: 'Ryszard Sikora',
    }, {
      creationDate: new Date(2018, 2, 1, 22, 23),
      id: '1243',
      logo: ImageProjectLogo,
      name: '3Streamify',
      owner: 'Ryszard Sikora',
    }, {
      creationDate: new Date(2018, 1, 10, 22, 23),
      id: '1243',
      logo: ImageProjectLogo,
      name: '4Streamify',
      owner: 'Ryszard Sikora',
    }, {
      creationDate: new Date(2018, 11, 5, 22, 23),
      id: '1243',
      logo: ImageProjectLogo,
      name: 'Streamify',
      owner: 'Ryszard Sikora',
    }, {
      creationDate: new Date(2018, 1, 1, 22, 23),
      id: '1243',
      logo: ImageProjectLogo,
      name: 'Streamify',
      owner: 'Ryszard Sikora',
    }],
    type: Type.all,
  };

  public render () {
    return (
      <div className='DashboardView'>
        <Header title='PROJECTS'/>
        <div className='DashboardView-content'>
          <div className='DashboardView-controlPanel'>
            {this.renderControls(
              'TYPE',
              [Type.your, Type.other],
              this.state.type,
              (type: Type) => this.setState({ type }),
              () => this.setState({ type: Type.all }),
            )}
            {this.renderControls(
              'ORDER BY',
              [OrderBy.name, OrderBy.updateDate],
              this.state.orderBy,
              (orderBy: OrderBy) => this.setState({ orderBy }),
            )}
          </div>
          <div className='DashboardView-projects'>
            <div className='DashboardView-tileWrapper'>
              <NavLink to='/projects/new' className='DashboardView-newProject'>
                <MdCreateNewFolder size={60} />
                <h1>CREATE NEW PROJECT</h1>
              </NavLink>
            </div>
            {this.renderProjects()}
          </div>
        </div>
      </div>
    );
  }

  private renderControls = (
    title: string,
    options: string[],
    selected: string,
    setType: (type: string) => void,
    resetType?: () => void,
  ) => (
    <div className='DashboardView-controls'>
      <h1>{title}</h1>
      <div className='DashboardView-buttons'>
        {options.map(name => (
          <button
            key={name}
            className={classNames('DashboardView-button', selected === name && 'DashboardView-button--selected')}
            onClick={resetType && selected === name ? resetType : () => setType(name)}
          >
            {name}
          </button>
        ))}
      </div>
    </div>
  )

  private renderProjects = () => {
    return this.getProjects().map((project, idx) => (
      <div key={project.name + idx} className='DashboardView-tileWrapper'>
        <div className='DashboardView-projectWrapper'>
          <div className='DashboardView-project'>
            <h1>{project.name}</h1>
            <h2>{`Created by ${project.owner}, ${formatDate(project.creationDate)}`}</h2>
            <h3>{`ID: ${project.id}`}</h3>
            <div className='DashboardView-projectLogo'>
              <img src={project.logo} alt='' />
            </div>
            <NavLink
              className='DashboardView-projectLink'
              to={`/project/${project.id}`}
            >
              <MdKeyboardArrowRight />
            </NavLink>
          </div>
        </div>
      </div>
    ));
  }

  private getProjects() {
    const defaultOwner = 'Ryszard Sikora';

    const { projects, orderBy, type } = this.state;
    return projects
      .filter(({ owner }) => {
        if (type === Type.other) {
          return owner !== defaultOwner;
        }
        if (type === Type.your) {
          return owner === defaultOwner;
        }
        return true;
      })
      .sort((a, b) => {
        if (orderBy === OrderBy.name) {
          return stringComparator(a.name, b.name);
        }
        if (orderBy === OrderBy.updateDate) {
          return dateComparator(a.creationDate, b.creationDate);
        }
        return a > b ? 1 : a === b ? 0 : -1;
      });
  }
}

const dateComparator = (a: Date, b: Date) => a.getTime() > b.getTime() ? 1 : a.getTime() === b.getTime() ? 0 : -1;
const stringComparator = (a: string, b: string) => a.localeCompare(b);

interface Project {
  name: string;
  owner: string;
  id: string;
  creationDate: Date;
  logo: string;
}

enum Type {
  your = 'YOUR',
  other = 'OTHER',
  all = 'ALL',
}

enum OrderBy {
  name = 'NAME',
  updateDate = 'UPDATE DATE',
}

interface State {
  type: Type;
  orderBy: OrderBy;
  projects: Project[];
}

const formatDate = (date: Date) => {
  const monthNames = [
    'January', 'February', 'March',
    'April', 'May', 'June', 'July',
    'August', 'September', 'October',
    'November', 'December',
  ];

  return `${date.getDate()} ${monthNames[date.getMonth()]} ${date.getFullYear()}`;
};
