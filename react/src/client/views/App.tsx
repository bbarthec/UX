import React from 'react';
import {
  Redirect,
  Route,
  Switch,
} from 'react-router';

import { Navbar } from './components';
import DashboardView from './DashboardView';
import ProjectView from './ProjectView';
import View404 from './View404';

import './App.scss';

export default class App extends React.Component<{}, {}> {
  public render() {
    return (
      <div className='App'>
        <Navbar />
        <Switch>
          <Redirect exact path='/' to='/dashboard' />
          <Route path='/dashboard' component={DashboardView} />
          <Route path='/project/:id' component={ProjectView} />
          <Route path='*' component={View404} />
        </Switch>
      </div>
    );
  }
}
