import React, { ChangeEvent } from 'react';
import { MdCheck, MdEdit } from 'react-icons/lib/md';

import './Editable.scss';

interface Props {
  onEdit: (value: string) => void;
  value: string;
}

interface State {
  editing: boolean;
  value?: string;
}

export default class extends React.PureComponent<Props, State> {
  public state = {
    editing: false,
    value: undefined,
  };

  public render() {
    return (
      <div className='Editable'>
        { this.state.editing
          ? this.renderEdit()
          : this.renderContent()
        }
      </div>
    );
  }

  private handleChange = (event: ChangeEvent<HTMLTextAreaElement>) => this.setState({ value: event.target.value });

  private renderEdit = () => [
    <textarea
      className='Editable-input'
      key='input'
      value={this.state.value || this.props.value}
      onChange={this.handleChange}
    />,
    <button className='Editable-button' key='button' onClick={() => {
      const { value } = this.state;
      this.setState({ editing: false, value: undefined }, () => {
        this.props.onEdit(value || '');
      });
    }}>
      <MdCheck />
    </button>,
  ]

  private renderContent = () => [
    this.props.children,
    <button className='Editable-button' key='button' onClick={() => this.setState({ editing: true })}>
      <MdEdit />
    </button>,
  ]
}
