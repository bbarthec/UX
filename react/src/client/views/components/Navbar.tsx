import React from 'react';
import { MdNotifications, MdSearch } from 'react-icons/lib/md';
import { NavLink } from 'react-router-dom';

import Link from './Link';

import ImageLogo from '@client/assets/images/Logo.svg';

import './Navbar.scss';

export default () => (
  <div className='Navbar-wrapper'>
    <div className='Navbar'>
      <div className='Navbar-left'>
        <NavLink className='Navbar-brand' to='/dashboard'>
          <img src={ImageLogo} alt='' />
        </NavLink>
        <Link to='/dashboard'>
          DASHBOARD
        </Link>
        <Link to='/users'>
          USERS
        </Link>
        <Link to='/profile'>
          PROFILE
        </Link>
      </div>
      <div className='Navbar-right'>
        <Link to='/projects'>
          <MdSearch size={35} />
        </Link>
        <Link to='/notifications'>
          <MdNotifications size={35} />
        </Link>
      </div>
    </div>
  </div>
);
