import classNames from 'classnames';
import React from 'react';

import './Tabs.scss';

interface Props {
  selected: string;
  onTabClick: (tab: string) => void;
  className?: string;
}

export default class Tabs extends React.PureComponent<Props, {}> {
  public render() {
    return (
      <div className={classNames('Tabs', this.props.className)}>
        {this.renderLabels()}
        {this.renderTab()}
      </div>
    );
  }

  private renderLabels() {
    return (
      <div className='Tabs-labels'>
        {(this.props.children as React.ReactNodeArray).map((node, idx) => {
          const name = (node as { props: { name: string } }).props.name;
          return (
            <button
              key={idx}
              className={classNames('Tabs-label', name.toLowerCase() === this.props.selected && 'Tabs-label--active')}
              onClick={() => {
                this.props.onTabClick(name.toLowerCase());
              }}
            >
              {name}
            </button>
          );
        })}
      </div>
    );
  }

  private renderTab() {
    return (this.props.children as React.ReactNodeArray)
      .find(node => (node as { props: { name: string } }).props.name.toLowerCase() === this.props.selected);
  }
}
