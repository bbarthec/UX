import React from 'react';

import './Header.scss';

export default ({ title }: { title: string }) => (
  <div className='Header-wrapper'>
    <div className='Header'>
      <h1>{title}</h1>
    </div>
  </div>
);
