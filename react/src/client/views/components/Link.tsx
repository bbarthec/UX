import { LocationDescriptor } from 'history';
import React from 'react';
import { NavLink } from 'react-router-dom';

import './Link.scss';

export default ({ children, to }: { children: React.ReactNode, to: LocationDescriptor }) => (
  <NavLink activeClassName='Link-active' className='Link' to={to}>
    {children}
  </NavLink>
);
