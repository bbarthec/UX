import React from 'react';

import { Action, Event } from './ProjectView';

import './HistoryTab.scss';

interface Props {
  events: Event[];
}

export default class HistoryTab extends React.PureComponent<Props, {}> {
  private static defaultProps = {
    name: 'HISTORY',
  };

  public render() {
    return (
      <div className='HistoryTab'>
        <h1>THIS WEEK</h1>
        {this.renderEvents(
          this.props.events.filter((({ date }) => date.getTime() > new Date().getTime() - 7 * 24 * 60 * 60 * 1000)))}
        <h1>THIS MONTH</h1>
        {this.renderEvents(
          this.props.events.filter((({ date }) =>
            date.getTime() > new Date().getTime() - 30 * 24 * 60 * 60 * 1000
              && date.getTime() < new Date().getTime() - 7 * 24 * 60 * 60 * 1000)))}
        <h1>BEFORE</h1>
        {this.renderEvents(this.props.events.filter((({ date }) =>
          date.getTime() < date.getTime() - 30 * 24 * 60 * 60 * 1000)))}
      </div>
    );
  }

  private renderEvents = (events: Event[]) => !events.length ? (
    <p>No notification to show</p>
  ) : events.map((event, idx) => (
    <p key={`${event.date.getTime()}_${idx}`}>
      <em>{event.owner}</em>
      &nbsp;
      { event.action === Action.invite
        ? 'has invited'
        : event.action === Action.remove
          ? 'has removed'
          : 'has performed unknown action towards'
      }
      &nbsp;
      <em>{event.target}</em>
      &nbsp;
      { event.action === Action.invite
        ? 'to the project!'
        : event.action === Action.remove
          ? 'from the project!'
          : '!'
      }
      <br />
      {Math.ceil((Date.now() - event.date.getTime()) / (60 * 60 * 1000))} hours ago
    </p>
  ))
}
