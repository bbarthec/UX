// IMAGES
declare module '*.png';
declare module '*.svg';
declare module '*.jpg';
// FONTS
declare module '*.fnt';
